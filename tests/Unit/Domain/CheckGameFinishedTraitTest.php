<?php

namespace Tests\Unit\Domain;

use App\Domain\CheckGameFinishedTrait;
use App\Domain\PlayerValueObject;
use Tests\TestCase;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
class CheckGameFinishedTraitTest extends TestCase
{
    use CheckGameFinishedTrait;

    private function getBoards() : array
    {
        return [
            'gameIsOver' => [
                [
                    1, 2, 2,
                    2, 1, 2,
                    1, 1, 2
                ],[
                    2, 1, 1,
                    2, 2, 1,
                    1, 1, 1
                ],[
                    2, 2, 2,
                    1, 1, 1,
                    1, 1, 2
                ],[
                    1, 2, 1,
                    2, 2, 2,
                    2, 1, 1
                ]
            ],
            'gameDidNotFinish' => [
                [
                    1, 2, 2,
                    0, 1, 2,
                    1, 1, 0
                ],[
                    0, 1, 0,
                    2, 0, 1,
                    1, 0, 1
                ],[
                    2, 0, 2,
                    1, 0, 1,
                    0, 0, 2
                ],[
                    0, 0, 0,
                    0, 0, 0,
                    0, 0, 0
                ]
            ]
        ];
    }

    /**
     * @test
     */
    public function gameISOver()
    {
        foreach ($this->getBoards()['gameIsOver'] as $board) {
            $this->assertTrue($this->wasFinished($board, PlayerValueObject::X_PLAYER));
        }
    }

    public function gameDidNotFinish()
    {
        foreach ($this->getBoards()['gameDidNotFinish'] as $board) {
            $this->assertTrue($this->wasFinished($board, PlayerValueObject::O_PLAYER));
        }
    }
}