<?php

namespace Tests\Unit\Domain;

use App\Domain\PlayerValueObject;
use App\Domain\RobotPlayerTrait;
use Tests\TestCase;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
class RobotPlayerTraitTest extends TestCase
{
    use RobotPlayerTrait;

    /**
     * @return array
     */
    private function getBoards() : array
    {
        return [
            PlayerValueObject::X_PLAYER => [
                [
                    'robot' => [
                        2, 0, 2,
                        2, 1, 1,
                        1, 2, 1
                    ],
                    'move' => [
                        2, 1, 2,
                        2, 1, 1,
                        1, 2, 1
                    ]
                ],
                [
                    'robot' => [
                        2, 1, 2,
                        2, 1, 1,
                        1, 0, 2
                    ],
                    'move' => [
                        2, 1, 2,
                        2, 1, 1,
                        1, 1, 2
                    ]
                ],
                [
                    'robot' => [
                        2, 1, 2,
                        2, 1, 0,
                        2, 1, 2
                    ],
                    'move' => [
                        2, 1, 2,
                        2, 1, 1,
                        2, 1, 2
                    ]
                ],
                [
                    'robot' => [
                        1, 1, 2,
                        2, 1, 2,
                        0, 1, 2
                    ],
                    'move' => [
                        1, 1, 2,
                        2, 1, 2,
                        1, 1, 2
                    ]
                ]
            ],

            PlayerValueObject::O_PLAYER => [
                [
                    'robot' => [
                        1, 1, 2,
                        2, 0, 1,
                        1, 2, 1
                    ],
                    'move' => [
                        1, 1, 2,
                        2, 2, 1,
                        1, 2, 1
                    ]
                ],
                [
                    'robot' => [
                        1, 1, 2,
                        0, 1, 1,
                        1, 2, 2
                    ],
                    'move' => [
                        1, 1, 2,
                        2, 1, 1,
                        1, 2, 2
                    ]
                ],
                [
                    'robot' => [
                        2, 2, 1,
                        2, 1, 2,
                        2, 0, 2
                    ],
                    'move' => [
                        2, 2, 1,
                        2, 1, 2,
                        2, 2, 2
                    ]
                ],
                [
                    'robot' => [
                        1, 0, 2,
                        2, 1, 1,
                        2, 1, 2
                    ],
                    'move' => [
                        1, 2, 2,
                        2, 1, 1,
                        2, 1, 2
                    ]
                ]
            ]
        ];
    }

    /**
     * @param int $player
     * @throws \PHPUnit\Framework\ExpectationFailedException
     * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
     */
    public function makeAMove(int $player)
    {
        $boards = $this->getBoards()[$player];

        foreach ($boards as $board) {
            $match = $this->getRandPosition(
                $board['robot'],
                $player
            );

            $this->assertEquals($board['move'], $match);
        }
    }

    /**
     * @test
     */
    public function makeAMoveXPlayer()
    {
        $this->makeAMove(PlayerValueObject::X_PLAYER);
    }

    /**
     * @test
     */
    public function makeAMoveOPlayer()
    {
        $this->makeAMove(PlayerValueObject::O_PLAYER);
    }
}
