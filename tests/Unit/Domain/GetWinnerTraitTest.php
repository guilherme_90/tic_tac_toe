<?php

namespace Tests\Unit\Domain;

use App\Domain\GetWinnerTrait;
use App\Domain\PlayerValueObject;
use Tests\TestCase;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
class GetWinnerTraitTest extends TestCase
{
    use GetWinnerTrait;

    const NO_WINNER = 0;

    /**
     * @return array
     */
    private function getMatches() : array
    {
        return [
            PlayerValueObject::X_PLAYER => [
                [ // left diagonal
                    1, 2, 2,
                    2, 1, 2,
                    1, 2, 1,
                ],
                [ // right diagonal
                    2, 2, 1,
                    2, 1, 2,
                    1, 2, 1,
                ],
                [ // first row
                    1, 1, 1,
                    2, 1, 2,
                    1, 2, 1,
                ],
                [ // second row
                    1, 2, 2,
                    1, 1, 1,
                    2, 1, 2,
                ],
                [ // third row
                    1, 2, 2,
                    2, 2, 1,
                    1, 1, 1,
                ],
                [ // first col
                    1, 1, 2,
                    1, 2, 1,
                    1, 2, 2,
                ],
                [ // second col
                    2, 1, 2,
                    1, 1, 2,
                    2, 1, 1,
                ],
                [ // third col
                    2, 2, 1,
                    1, 2, 1,
                    2, 1, 1,
                ]
            ],

            PlayerValueObject::O_PLAYER => [
                [ // left diagonal
                    2, 1, 1,
                    1, 2, 1,
                    1, 1, 2,
                ],
                [ // right diagonal
                    1, 1, 2,
                    1, 2, 1,
                    2, 1, 1,
                ],
                [ // first row
                    2, 2, 2,
                    2, 1, 1,
                    1, 1, 2,
                ],
                [ // second row
                    1, 1, 2,
                    2, 2, 2,
                    1, 2, 1,
                ],
                [ // third row
                    1, 1, 1,
                    1, 1, 1,
                    2, 2, 2,
                ],
                [ // first col
                    2, 1, 1,
                    2, 2, 1,
                    2, 2, 1,
                ],
                [ // second col
                    1, 2, 1,
                    2, 2, 1,
                    1, 2, 2,
                ],
                [ // third col
                    2, 1, 2,
                    1, 2, 2,
                    1, 1, 2,
                ]
            ],

            PlayerValueObject::NO_WINNER => [
                [ // left diagonal
                    0, 2, 2,
                    2, 0, 2,
                    1, 2, 0,
                ],
                [ // right diagonal
                    2, 2, 0,
                    2, 0, 2,
                    0, 2, 1,
                ],
                [ // first row
                    0, 0, 0,
                    2, 1, 2,
                    1, 2, 2,
                ],
                [ // second row
                    2, 1, 2,
                    0, 0, 0,
                    1, 2, 2,
                ],
                [ // third row
                    2, 0, 2,
                    0, 2, 2,
                    0, 1, 0,
                ],
                [ // first col
                    0, 2, 2,
                    0, 1, 1,
                    0, 2, 1,
                ],
                [ // second col
                    1, 0, 1,
                    2, 0, 2,
                    2, 0, 2,
                ],
                [ // third col
                    1, 2, 0,
                    2, 1, 0,
                    1, 2, 0,
                ]
            ]
        ];
    }

    /**
     * @test
     */
    public function playerXIsWinner()
    {
        $playerX = $this->getMatches()[PlayerValueObject::X_PLAYER];

        foreach ($playerX as $match) {
            $this->assertSame(PlayerValueObject::X_PLAYER, $this->isWinner($match));
        }
    }

    /**
     * @test
     */
    public function playerOIsWinner()
    {
        $playerX = $this->getMatches()[PlayerValueObject::O_PLAYER];

        foreach ($playerX as $match) {
            $this->assertSame(PlayerValueObject::O_PLAYER, $this->isWinner($match));
        }
    }

    /**
     * @test
     */
    public function noWinner()
    {
        foreach ($this->getMatches()[PlayerValueObject::NO_WINNER] as $match) {
            $this->assertSame(PlayerValueObject::NO_WINNER, $this->isWinner($match));
        }
    }
}