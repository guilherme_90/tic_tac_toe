<?php

namespace Tests\Unit\Domain;

use App\Domain\GetNextPlayerTrait;
use Tests\TestCase;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
class GetNextPlayerTraitTest extends TestCase
{
    use GetNextPlayerTrait;

    /**
     * @throws \PHPUnit\Framework\ExpectationFailedException
     * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
     *
     * @test
     */
    public function playerXIsTheNext()
    {
        $this->assertSame(1, $this->getNextPlayer(2));
    }

    /**
     * @throws \PHPUnit\Framework\ExpectationFailedException
     * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
     *
     * @test
     */
    public function playerOIsTheNext()
    {
        $this->assertSame(2, $this->getNextPlayer(1));
    }
}