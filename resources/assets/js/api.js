import axios from 'axios'

const URL_MATCHES = '/api/matches';

export default {
  matches: () => {
    return axios.get(URL_MATCHES)
  },
  
  match: ({id}) => {
    return axios.get(`${URL_MATCHES}/${id}`)
  },
  
  move: ({id, position, currentPlayer, isRobot}) => {
    return axios.put(`${URL_MATCHES}/${id}`, {
      position: position,
      currentPlayer: currentPlayer,
      isRobot: isRobot
    })
  },
  
  create: () => {
    return axios.post(URL_MATCHES)
  },
  
  destroy: ({id}) => {
    return axios.delete(`${URL_MATCHES}/${id}`)
  },
}