<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Matches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('matches', function (Blueprint $table) {
            $table->collation = 'utf8_general_ci';
            $table->charset = 'utf8';

            $table->uuid('id');
            $table->primary('id');
            $table->string('name', 20);
            $table->tinyInteger('next');
            $table->tinyInteger('winner')->default(0);
            $table->boolean('ended')->default(false);
            $table->text('board');

            $table->timestamp('created_at')
                ->nullable()
                ->default(\Illuminate\Support\Facades\DB::raw('CURRENT_TIMESTAMP'));

            $table->timestamp('updated_at')
                ->nullable()
                ->default(\Illuminate\Support\Facades\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->dateTime('finished_at')
                ->nullable()
                ->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('matches');
    }
}
