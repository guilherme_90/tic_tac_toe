<?php

use Faker\Generator as Faker;

$factory->define(App\Domain\Model\Matches::class, function (Faker $faker) {
    return [
        'id' => (string) \Ramsey\Uuid\Uuid::uuid4(),
        'name' => 'Match #' . random_int(1, 99999),
        'winner' => \App\Domain\PlayerValueObject::NO_WINNER,
        'next' => \App\Domain\PlayerValueObject::X_PLAYER,
        'ended' => false,
        'board' => json_encode(['board' => [
            0, 0, 0,
            0, 0, 0,
            0, 0, 0
        ]])
    ];
});
