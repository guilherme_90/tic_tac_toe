<?php

namespace App\Http\Controllers;

use App\Domain\Service\MatchesService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
class MakeMatchController extends Controller
{
    /**
     * @var MatchesService
     */
    private $matchesService;

    /**
     * @param MatchesService $matchesService
     */
    public function __construct(MatchesService $matchesService)
    {
        $this->matchesService = $matchesService;
    }

    /**
     * @param string  $id
     * @param Request $request
     * @return JsonResponse
     */
    public function process(string $id, Request $request) : JsonResponse
    {
        $data = $request->all();

        try {
            $match = $this->matchesService->move($id, $data);

            return \response()->json($match);
        } catch (\Exception $e) {
            return \response()->json($e->getMessage());
        }
    }
}