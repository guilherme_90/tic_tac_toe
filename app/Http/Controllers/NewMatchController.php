<?php

namespace App\Http\Controllers;

use App\Domain\Service\MatchesService;
use Illuminate\Http\JsonResponse;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
class NewMatchController extends Controller
{
    /**
     * @var MatchesService
     */
    private $matchesService;

    /**
     * @param MatchesService $matchesService
     */
    public function __construct(MatchesService $matchesService)
    {
        $this->matchesService = $matchesService;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function process() : JsonResponse
    {
        try {
            $this->matchesService->newMatch();

            $matches = $this->matchesService->matchesList();

            return \response()->json($matches);
        } catch (\Exception $e) {
            return \response()->json($e->getMessage());
        }
    }
}