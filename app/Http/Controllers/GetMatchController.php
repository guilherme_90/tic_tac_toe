<?php

namespace App\Http\Controllers;

use App\Domain\Service\MatchesService;
use Illuminate\Http\JsonResponse;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
class GetMatchController extends Controller
{
    /**
     * @var MatchesService
     */
    private $matchesService;

    /**
     * @param MatchesService $matchesService
     */
    public function __construct(MatchesService $matchesService)
    {
        $this->matchesService = $matchesService;
    }

    /**
     * @param $id
     * @throws \Exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function process($id) : JsonResponse
    {
        $match = $this->matchesService->getMatchById($id);

        try {
            return response()->json($match);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }
    }
}