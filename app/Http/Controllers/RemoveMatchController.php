<?php

namespace App\Http\Controllers;

use App\Domain\Service\MatchesService;
use Illuminate\Http\JsonResponse;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
class RemoveMatchController extends Controller
{
    /**
     * @var MatchesService
     */
    private $matchesService;

    /**
     * @param MatchesService $matchesService
     */
    public function __construct(MatchesService $matchesService)
    {
        $this->matchesService = $matchesService;
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function process(string $id) : JsonResponse
    {
        try {
            $this->matchesService->remove($id);

            $matches = $this->matchesService->matchesList();

            return \response()->json($matches, 200);
        } catch (\Exception $e) {
            return \response()->json($e->getMessage(), $e->getCode());
        }
    }
}