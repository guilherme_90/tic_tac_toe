<?php

namespace App\Http\Controllers;

use App\Domain\Service\MatchesService;
use Illuminate\Http\JsonResponse;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
class MatchesListController extends Controller
{
    /**
     * @var MatchesService
     */
    private $matchesService;

    /**
     * @param MatchesService $matchesService
     */
    public function __construct(MatchesService $matchesService)
    {
        $this->matchesService = $matchesService;
    }

    /**
     * @return JsonResponse
     */
    public function process() : JsonResponse
    {
        $matches = $this->matchesService->matchesList();

        return response()->json($matches);
    }
}