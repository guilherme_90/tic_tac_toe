<?php

namespace App\Providers;

use App\Domain\Repository\MatchesRepository;
use App\Domain\Repository\MatchesRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MatchesRepositoryInterface::class, MatchesRepository::class);
    }
}
