<?php

namespace App\Domain;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
trait RobotPlayerTrait
{
    /**
     * @param array $board
     * @param int   $nextPlayer
     * @return array
     */
    public function getRandPosition(array $board, int $nextPlayer) : array
    {
        $positions = [];
        foreach ($board as $key => $item) {
            if ($board[$key] === 0) {
                $positions[$key] = $key;
            }
        }

        $position = count($positions) > 1 ?
            array_rand($positions, 1)
            : key($positions);

        $board[$position] = $nextPlayer;

        return $board;
    }
}