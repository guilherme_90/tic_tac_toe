<?php

namespace App\Domain\Service;

use App\Domain\CheckGameFinishedTrait;
use App\Domain\GetNextPlayerTrait;
use App\Domain\GetWinnerTrait;
use App\Domain\Model\Matches;
use App\Domain\PlayerValueObject;
use App\Domain\Repository\MatchesRepositoryInterface;
use App\Domain\RobotPlayerTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Ramsey\Uuid\Uuid;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
class MatchesService
{
    use GetNextPlayerTrait;
    use GetWinnerTrait;
    use RobotPlayerTrait;
    use CheckGameFinishedTrait;

    /**
     * @var MatchesRepositoryInterface
     */
    private $matchesRepository;

    /**
     * @param MatchesRepositoryInterface $matchesRepository
     */
    public function __construct(MatchesRepositoryInterface $matchesRepository)
    {
        $this->matchesRepository = $matchesRepository;
    }

    /**
     * @return Collection
     */
    public function matchesList() : \Illuminate\Support\Collection
    {
        return $this->matchesRepository->matchesList();
    }

    /**
     * @param string $matchId
     * @return Matches
     * @throws \Exception
     */
    public function getMatchById(string $matchId) : Matches
    {
        if (! Uuid::isValid($matchId)) {
            throw new \Exception(
                sprintf('Match %s invalid', $matchId), 400
            );
        }

        $match = $this->matchesRepository->getMatchById($matchId);

        if (! $match) {
            throw new \Exception(
                sprintf('Match %s does not exists.', $matchId), 404
            );
        }

        return $match;
    }

    /**
     * @param string $matchId
     * @param array  $data
     * @return Matches
     * @throws \Exception
     */
    public function move(string $matchId, array $data) : Matches
    {
        $match = $this->getMatchById($matchId);
        $board = $match['board'];

        if ($data['isRobot']) {
            $board = $this->getRandPosition($board, $data['currentPlayer']);
        }

        if (! $data['isRobot']) {
            $board[$data['position']] = $data['currentPlayer'];
        }

        $winner = $this->isWinner($board);
        $data['winner'] = $winner;
        $data['board'] = json_encode(['board' => $board]);
        $data['nextPlayer'] = $this->getNextPlayer($data['currentPlayer']);
        $data['ended'] = $this->wasFinished($board, $winner);

        if ($this->wasFinished($board, $winner)) {
            $data['finished_at'] = Carbon::now();
        }

        return $this->matchesRepository->move($match, $data);
    }

    /**
     * @return void
     */
    public function newMatch() : void
    {
        $this->matchesRepository->newMatch([
            'id'=> (string) Uuid::uuid4(),
            'name' => 'Match #' . random_int(1, 99999),
            'winner' => PlayerValueObject::NO_WINNER,
            'next' => PlayerValueObject::X_PLAYER,
            'ended' => false,
            'board' => json_encode(['board' => [
                0, 0, 0,
                0, 0, 0,
                0, 0, 0
            ]])
        ]);
    }

    /**
     * @param string $matchId
     * @throws \Exception
     */
    public function remove(string $matchId) : void
    {
        if (! Uuid::isValid($matchId)) {
            throw new \Exception(
                sprintf('Id %s invalid', $matchId), 400
            );
        }

        $matches = $this->getMatchById($matchId);
        $this->matchesRepository->remove($matches);
    }
}