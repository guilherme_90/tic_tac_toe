<?php

namespace App\Domain\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use PhpParser\JsonDecoder;
use Psy\Util\Json;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
class Matches extends Model
{
    /**
     * @var string
     */
    public $table = 'matches';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'next', 'winner', 'ended', 'board', 'finished_at'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'finished_at'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'updated_at', 'created_at'
    ];

    /**
     * @param string $value
     * @return array
     * @throws \RuntimeException
     */
    public function getBoardAttribute(string $value) : array
    {
        $decoded = (new JsonDecoder())->decode($value);

        return $decoded['board'];
    }
}