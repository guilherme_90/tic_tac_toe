<?php

namespace App\Domain;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
trait GetNextPlayerTrait
{
    /**
     * @param int $currentPlayer
     * @return int
     */
    public function getNextPlayer(int $currentPlayer) : int
    {
        return $currentPlayer === PlayerValueObject::O_PLAYER
            ? PlayerValueObject::X_PLAYER
            : PlayerValueObject::O_PLAYER;
    }
}