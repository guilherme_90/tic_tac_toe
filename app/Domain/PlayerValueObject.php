<?php

namespace App\Domain;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
abstract class PlayerValueObject
{
    const X_PLAYER = 1;

    const O_PLAYER = 2;

    const NO_WINNER = 0;
}