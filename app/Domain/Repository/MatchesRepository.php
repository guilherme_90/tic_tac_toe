<?php

namespace App\Domain\Repository;

use App\Domain\Model\Matches;
use Illuminate\Support\Collection;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
class MatchesRepository implements MatchesRepositoryInterface
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $matches;

    /**
     * @param Matches $matches
     */
    public function __construct(Matches $matches)
    {
        $this->matches = $matches->newQuery();
    }

    /**
     * @return Collection
     */
    public function matchesList(): Collection
    {
        return $this->matches
            ->orderBy('name', 'ASC')
            ->get();
    }

    /**
     * @inheritdoc
     */
    public function getMatchById(string $matchId) : ?Matches
    {
        return $this->matches->find($matchId);
    }

    /**
     * @inheritdoc
     */
    public function remove(Matches $matches) : void
    {
        $matches->delete();
    }

    /**
     * @inheritdoc
     */
    public function newMatch(array $data) : void
    {
        $this->matches->create($data);
    }

    /**
     * @inheritdoc
     */
    public function move(Matches $matches, array $data) : Matches
    {
        $matches->update([
            'next' => $data['nextPlayer'],
            'winner' => $data['winner'],
            'board' => $data['board'],
            'ended' => $data['ended'],
            'finished_at' => $data['finished_at'] ?? null
        ]);

        return $matches;
    }
}