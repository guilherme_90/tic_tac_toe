<?php

namespace App\Domain\Repository;

use App\Domain\Model\Matches;
use Illuminate\Support\Collection;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
interface MatchesRepositoryInterface
{
    /**
     * @return Collection
     */
    public function matchesList() : Collection;

    /**
     * @param string $matchId
     * @return Matches|null
     */
    public function getMatchById(string $matchId) : ?Matches;

    /**
     * @param Matches $matches
     */
    public function remove(Matches $matches) : void;

    /**
     * create new match
     *
     * @param array $data
     * @return void
     */
    public function newMatch(array $data) : void;

    /**
     * @param Matches $matches
     * @param array   $data
     * @return Matches
     */
    public function move(Matches $matches, array $data) : Matches;
}