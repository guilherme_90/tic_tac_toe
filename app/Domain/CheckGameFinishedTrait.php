<?php

namespace App\Domain;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
trait CheckGameFinishedTrait
{
    /**
     * @param array $board
     * @param int   $winner
     * @return bool
     */
    public function wasFinished(array $board, int $winner) : bool
    {
        if ($winner !== 0) {
            return true;
        }

        /**
         * @param int $key
         * @param int $value
         */
        $filter = array_filter($board, function ($key, $value) {
            return $key === 0;
        }, ARRAY_FILTER_USE_BOTH );

        if (empty($filter)) {
            return true;
        }

        return false;
    }
}