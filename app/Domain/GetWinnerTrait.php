<?php

namespace App\Domain;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */
trait GetWinnerTrait
{
    /**
     * @param array $board
     * @return int
     */
    public function isWinner(array $board = []) : int
    {
        // positions
        $positions = [
            'diagonal' => [
                [0, 4, 8], // left to right
                [2, 4, 6], // right to left
            ],
            'row' => [
                [0, 1, 2], // first row
                [3, 4, 5], // second row
                [6, 7, 8], // third row
            ],
            'col' => [
                [0, 3, 6], // first col
                [1, 4, 7], // second col
                [2, 5, 8] // third col
            ]
        ];

        $matches = [];
        $winner = 0;

        foreach ($board as $position => $match) {
            // diagonal
            if (in_array($position, $positions['diagonal'][0])) {
                $matches[$match]['diagonal'][0][] = $position;
            }

            if (in_array($position, $positions['diagonal'][1])) {
                $matches[$match]['diagonal'][1][] = $position;
            }

            // row
            if (in_array($position, $positions['row'][0])) {
                $matches[$match]['row'][0][] = $position;
            }

            if (in_array($position, $positions['row'][1])) {
                $matches[$match]['row'][1][] = $position;
            }

            if (in_array($position, $positions['row'][2])) {
                $matches[$match]['row'][2][] = $position;
            }

            // col
            if (in_array($position, $positions['col'][0])) {
                $matches[$match]['col'][0][] = $position;
            }

            if (in_array($position, $positions['col'][1])) {
                $matches[$match]['col'][1][] = $position;
            }

            if (in_array($position, $positions['col'][2])) {
                $matches[$match]['col'][2][] = $position;
            }
        }

        if (count($matches) > 0) {
            /**
             * @param int $player
             */
            $getWinner = function(int $player) use (
                $matches,
                $positions,
                &$winner
            ) {
                foreach ($matches as $match => $item) {
                    if ($match === $player) {

                        /**
                         * Diagonals
                         */
                        if (isset($item['diagonal'][0]) && $item['diagonal'][0] === $positions['diagonal'][0]) {
                            $winner = $match;
                        }

                        if (isset($item['diagonal'][1]) && $item['diagonal'][1] === $positions['diagonal'][1]) {
                            $winner = $match;
                        }

                        /**
                         * Rows
                         */
                        if (isset($item['row'][0]) && $item['row'][0] === $positions['row'][0]) {
                            $winner = $match;
                        }

                        if (isset($item['row'][1]) && $item['row'][1] === $positions['row'][1]) {
                            $winner = $match;
                        }

                        if (isset($item['row'][2]) && $item['row'][2] === $positions['row'][2]) {
                            $winner = $match;
                        }

                        /**
                         * Cols
                         */
                        if (isset($item['col'][0]) && $item['col'][0] === $positions['col'][0]) {
                            $winner = $match;
                        }

                        if (isset($item['col'][1]) && $item['col'][1] === $positions['col'][1]) {
                            $winner = $match;
                        }

                        if (isset($item['col'][2]) && $item['col'][2] === $positions['col'][2]) {
                            $winner = $match;
                        }
                    }
                }
            };

            $getWinner(1);
            $getWinner(2);
        }

        return $winner;
    }
}