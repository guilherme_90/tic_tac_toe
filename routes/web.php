<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'index');

Route::prefix('api')->group(function () {
    Route::get('matches', 'MatchesListController@process')->name('matches');
    Route::get('matches/{id}', 'GetMatchController@process')->name('match.get');

    Route::put('matches/{id}', 'MakeMatchController@process')->name('match.makeMatch');
    Route::post('matches', 'NewMatchController@process')->name('match.newMatch');

    Route::delete('matches/{id}', 'RemoveMatchController@process')->name('match.remove');
});

